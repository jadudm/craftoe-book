\laboratory{Measurement and Destruction}

In this lab, you'll begin working with your multimeter to make a variety of measurements. You'll use the meter to measure resistance, voltage, and current in a number of different contexts. As a result, this will also be your first exposure to building circuits.

When we're done measuring things in safe ways, we'll then explore a few ways to destroy things with electricity. This is, of course, what we usually \emph{don't} want to do. But, it is important to know how components fail, and by destroying a few, we'll know what \emph{not} to do when we want our circuits to work correctly.

\section{Background Reading}

You should have read the previous chapter. In addition to reading the chapter, you should read or view the following.

\begin{enumerate}
  \item \href{https://www.youtube.com/watch?v=ZBbgiBU96mM}{How to use a Multimeter for Beginners} (30m) \\ This video explores the basic use of a Fluke 117 multimeter.

  \item  \href{https://www.ifixit.com/Guide/How+To+Use+A+Multimeter/25632}{How to Use a Multimeter} \\ This webpage from iFixit (a wholly remarkable site) describes how to measure continuity, voltage, and resistence---the three kinds of measurement you will be engaging in with electrical circuits all the time.

  \item \href{https://www.youtube.com/watch?v=VpQVQv5DSe8}{How to Calculate the Mean and Standard Deviation} \\ A refresher on two basic statistical calculations that we will often carry out on a set of readings.

\end{enumerate}

\section{Pre-Lab Questions}

\begin{enumerate}
  \item On what mode or setting should you leave your multimeter when you are not actively using it?

  \item Where online can you find the manual for your multimeter? Download it and create a folder for documentation regarding the tools you use.

  \item What is the maximum DC voltage your meter can measure?

  \item What is the maximum current your meter can measure?

  \item What is the best way to store the probes and leads when you are done working with the multimeter?

  \item Is it better to connect your power supply to your project before setting the voltage, or after? Why is this good practice?

  \item When attempting to intentionally destroy electrical components, what precautions do you think you should take?
\end{enumerate}

Your pre-lab questions should be written out and answered in your laboratory notebook for you to refer back to.

\section{The Lab}

This laboratory involves a series of explorations that will expose you to ideas and concepts in the building and testing (and sometimes destroying) of electronic circuits. In this laboratory, you will be explicitly prompted at points to record or otherwise make note of phenomena that you observe; over time, it is expected that you will learn to engage in this measurement, recording, and reflection with less and less prompting, as it will become part of your normal practice.

\subsection{Measuring Continuity}

Continuity testing is incredibly common in electrical work---because it tells us whether two things are connected (or not). Because building a circuit often involves connecting things, we are often very concerned with making sure that we have achieved our goals.

With your partner, explore the lab in terms of electrical continuity. Where can you find continuity, and where can you not? In each case, make sure to discuss what you expect before you take your measurement (and make note), and then what you observe. After exploring the laboratory (and/or further afield) in terms of electrical continuity, discuss with your partner and make notes about things you were surprised by in your explorations.


\subsection{Testing our Tools}

One power supply has been set up in the laboratory to a fixed voltage. Measure that voltage, and write down your name and what your multimeter reports on the card next to the power supply.

After everyone has measured this power supply, make sure to copy the readings into your notebook.

\subsection{Measuring Voltage}

For this part of the laboratory, we're going to wander around measuring some voltages. Get your trusty multimeter, and in each case, think about 1) what setting you will need to use to conduct your measurements, and 2) what safety precautions should be taken before proceeding. In each case, make note of what you \emph{expect} to measure, what you \emph{actually} measured, how you measured it, and what value(s) you obtained from your measurements.

\begin{description}
  \item[AA Batteries.] What voltage do you read from an AA battery? Do you get the same reading from every battery?

  \item[9V Batteries.] See if you can find a 9V battery somehwere; what do you read? Is this surprising?

  \item[Laptop Battery.] Some laptops have removable batteries. Can you remove a laptop battery and measure the voltage being output by the battery?

  \item[Car Battery.] There's bound to be one around somewhere...

  \item[Portable Drill Battery]. No self-respecting lab would be without a power drill.

  \item[Power Supply.] Get your bench-top power supply out. Does it report voltages accurately?

  \item[Aging Power Supply.] Find an old power supply; does it report voltages accurately?

  \item[A wall outlet.] Before proceeding, make sure you and your partner are confident in your tools. Are the cables in good condition? Is the meter in good condition? In the US, an outlet typically has two prongs and a ground; draw a diagram of the outlet in your lab notebook, and note what readings you get when you measure between the prongs and between each prong and ground.
\end{description}

\subsection{Measuring Resistance}

From the parts bin, select five different values of resistor: a 220\ohm, a 1K\ohm (or 1000\ohm), a 10K\ohm, a 2.2K\ohm, and a 1M\ohm resistor. (You may, if you wish, select different values, but make sure to include a 1K and 10K in your selection.) You will want three of each resistor---make sure not to mix all of these up!

For each resistor, make note of its value (as indicated on the parts bin) and the color bands on the resistor. Then, use your multimeter to measure the resistance of each resistor, and note these in your notebook.

\begin{enumerate}
  \item What is the mean resistance you measured for each group of resistors? (No more than 2 decimal places accuracy.)

  \item What is the standard deviation in the resistance for group of resistors?

  \item Are all of your measurements within 5\% of the stated value of each resistor? Are they all within 10\% of the stated value?
\end{enumerate}

You can either calculate the mean and standard deviation by hand (it's not that hard) or with a spreadsheet. ``By hand'' here probably means ``with a calculator.''

\subsection{Destroying Resistors}

For our final exploration, you will need to distribute some data amongst you and some friends. You will collect one data point, and one of your friends will pick one data point, and so on, until the group of you have 10-12 data points combined.

Each of you should do the following:

\begin{enumerate}
  \item Pick a resistor in the range 0-1000\ohm.
  \item Turn your power supply on.
  \item Dial your power supply down to 0V.
  \item Turn your power supply off.
  \item Connect your power supply directly to the resistor... from power, to the resistor, to ground. Use aligator clips.
  \item Turn the power supply on.
  \item Slowly turn the power up, using the fine adjustment on your power supply, until your resistor dies.
\end{enumerate}

You should only choose one resistor, and destroy only one resistor. Each group will, in this way, collect one data point. Along with your friends, you should end up with a spread of resistance values from 10\ohm all the way through to 1000\ohm (or 1K\ohm), and a spread of voltages at which the resistors died. For example, if a 10\ohm resistor dies at 1V, and a 560\ohm resistor dies at 2V, you're starting to develop a dataset that can be analyzed...

Hopefully, you have around 10-12 friends who can do this experiment with you and share their data. (You could do it all by yourself, but that would be no fun.)

\section{Reflection}

Your first laboratory exploration involved heavy use of the multimeter. Throughout your studies of electricity and electronics, and in any future work you do with electrical systems, a multimeter will be your primary tool for testing systems before you work on them and after.

To close the laboratory, answer the following questions in your notebook.

\begin{enumerate}
  \item Did any of your measurements appear more dangerous than others? Why or why not?

  \item What was the mean and standard deviation of the readings taken on the common power supply? Are the meters in our toolboxes consistent? Out of calibration? Is the variation tolerable? Why or why not?

  \item If you were to go back and do this exploration over again, what would you do differently?

  \item How much time did you spend on this laboratory, in and out of class?

\end{enumerate}

\section{Summarize and Submit}

At this point, you have a great deal of information in your lab notebook. Using a word processor, summarize your findings in a concise report.

Looking at your explorations, it looks like there were five primary areas of exploration. First, you explored the measurement of continuity, voltage, and resistance. For each of these, you should have a short section in your report that describes what each of these concepts is (to the best of your ability), what you did to carry out your analysis, and what you found. You might comment on what results were surprising, and posit a theory as to why you found the results that you did.

You then looked at some resistors, and calculated means and standard deviations. You should describe your method (your process), what you found, and do your best to interpret those results for the reader. Were your findings to be expected? Do you believe you lack data to make any conclusive statements? If so, discuss what you would do to improve your study and analysis of resistor values.

Finally, as a group, you burned out resistors. You should all have a list of resistance values and a list of voltages at which those resistances burned out. Carefully sketch, by hand, a graph that has those values on it. Is it a straight line? A curve? To the best of your ability, descirbe what your data tells you.

We are not concerned about introductions and conclusions at this point---just writing what we did, what we observed, and how we interpret our findings. (This is, practically speaking, the beginning of what it means to engage as a \emph{scientist}.)
