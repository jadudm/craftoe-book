
%----------------------------------------------------------------------------------------
%	CHAPTER 1
%----------------------------------------------------------------------------------------

\chapter{Power and Measurement}
\label{ch1:powerandmeasurement}

All circuits need power. The theory of what that means---to supply power to a circuit---is substantial, and could become the topic of a life of work and study. Electricians and electrical engineers both end up needing to know a great deal about electrical systems. Likewise, mechanics and automotive engineers (because many cars are battery powered) need to know about these kinds of systems as well. Software developers and computer scientists need to understand electronics for the simple reason that \emph{everything} they do involves a system that cannot function without electricity.

\begin{figure}
  \begin{centering}
    \includegraphics[width=\textwidth]{content/measurement/images/bkprecision-power-supply.png}
  \end{centering}
  \caption{A modern bench-top power supply.}
  \label{ch1:benchsupply}
\end{figure}

A \emph{power suppy\index{power supply}}, or more specifically in this case, a \emph{bench-top power supply\index{power supply!bench-top power supply}}, is a device that converts the alternating current\index{AC!alternating current} (or AC) that is delivered to us in the walls of our homes and workplaces into the direct current \index{DC!direct current} (or DC) that we need for modern electronic systems.\sidenote{The topic of direct current is the subject of an entire book, \href{http://www.allaboutcircuits.com/textbook/direct-current/}{All About Circuits, Volume I}. The second book, \href{http://www.allaboutcircuits.com/textbook/alternating-current/}{Volume II}, deals with AC. You will often be referred to these texts as a source of additional information.} A modern bench-top supply is pictured in Figure~\ref{ch1:benchsupply}.

\newthought{To use the power supply}, we need to set it to the correct voltage, and then connect it to our circuit. Every power supply will behave slightly differently---and some are downright confusing. Depicted above is a fairly standard, good quality supply. For all supplies, we recommend the following procedure:

\newpage

\begin{enumerate}
  \item While the unit is off, plug in your leads. These will have a banana plug on one end, and a clip of some sort on the other. (Figure~\vref{ch1:bananaplugs})
  \item Turn on the power supply. Adjust it to the voltage that you desire.
  \item \emph{If you are paranoid}, check that your power supply is correctly reporting its voltage by using a multimeter to check that it is emitting the correct voltage.
  \item Turn off the power supply. Connect it to your circuit. Make sure it is connected correctly.
  \item Turn on your power supply.
\end{enumerate}

% CC-BY https://flic.kr/p/j2ZGvi.
\begin{marginfigure}
  \includegraphics[]{content/measurement/images/banana-plugs-medium.jpg}
  \caption{Banana plugs.}
  \label{ch1:bananaplugs}
\end{marginfigure}

With a new power supply in good condition, or one that has been serviced recently (there should be a sticker on it somewhere indicating this), the power supply should provide a voltage and current that is accurately reported by its front panel. Because \textbf{the only safe way to work with electronics is with extreme paranoia}, we will introduce the next tool in our toolkit---the multimeter---right after a brief discussion about \emph{paranoia}.

\subsection{Paranoia: Another Word for Safety}

\newthought{Electrical systems are largely invisible}. You cannot, under normal circumstances, look at an electrical system and tell whether it is energized, whether it is a low-voltage and low-current system (like a cell phone) or if it is a high-voltage, high-current system (like what you find in the walls of a home). Therefore, whenever you are working with an electrical system, \textbf{you must always assume it is ``hot'', and that handling it incorrectly might kill you}.

\begin{marginfigure}
  \includegraphics[]{content/measurement/images/high-voltage.png}
  \caption{A high voltage warning sign.}
  \label{ch1:highvoltage}
\end{marginfigure}

This attitude is, admittedly, a bit overkill when working with a small, digital electronics project in the classroom. However, when working with any electrical systems in a house, or an unknown system that you have never seen before, you should approach your electronics work with the assumption that you could harm yourself, someone who might use the system (or service it) after you, or an innocent bystander if you do the wrong thing. This is obvious with home wiring, but even small-scale electronics (say, like cell phones powered by lithium polymer batteries) can explode dangerously when their electronics are shorted out or otherwise damaged.

For this reason, before we even build a circuit, we're going to begin learning how to use the \emph{multimeter}, which is the single most common, versatile, and important tool you can learn to use in the study of electronics.

\subsection{The Multimeter}

A multimeter---or ``multi-measurement tool''---is your first and last line of defense when working with electrical systems. It is your first and best tool for discovering:

\begin{itemize}
  \item Is an electrical circuit ``live'' or ``hot''
  \item Is an electrical circuit fully connected
  \item The resistance between two points in an electrical circuit
  \item The voltage between two points in an electrical circuit
  \item The current flowing through an electrical circuit
\end{itemize}

and, depending on the meter, many other functions.

\subsection{The Danger of Cheap Multimeters}

\begin{figure}
  \begin{centering}    \includegraphics[width=\textwidth]{content/measurement/images/dave-eevblog.jpg}
  \end{centering}
  \caption{Dave of EEVBlog Fame.}
  \label{ch1:dave}
\end{figure}

\begin{marginfigure}
  \includegraphics[]{content/measurement/images/cheap-multimeter.jpg}
  \caption{A cheap multimeter.}
  \label{ch1:cheapmultimeter}
\end{marginfigure}

It is possible to pick up a multimeter for a few dollars online or at any hardware store. It might look like something like Figure~\vref{ch1:cheapmultimeter}. The important thing to realize about a low-cost meter is that it is going to be made with low-quality components and minimal protection circuitry. Put another way, a cheap meter is a tool that you might be using to (attempt) to measure dangerous levels of voltage and current, and in doing so, put yourself at risk. It might read incorrectly (leading you to believe a circuit is safe to handle), or the meter might become damaged through use in ways that either harm you or, worse, lead it to behave incorrectly or in otherwise random ways.

On the EEVBlog, Dave from Down Under (Figure~\vref{ch1:dave}) delivers some devastating deconstructions of all kinds of electrical tools and systems; his video blog is really an excellent source of knowledge regarding the design and development of electronics in general. In particular, in his sixth episode, he speaks to why cheap multimeters are dangerous; you might take some time to watch it now (\href{https://youtu.be/8cNc5An0DLw}{EEVBlog Episode 6: Why Cheap Multimeters Suck}).

\bigskip
While you're watching, you should make some notes.

\begin{description}
  \item[Terminology?] What terminology does Dave use in his videos? Write down technical terms that you hear go by, but you aren't sure what they mean.

  \item[Components?] He references a number of electrical components inside the meters. Again, write those down for later lookup.

  \item[Safety?] What kind of safety concerns does Dave have regarding cheap multimeters?

  \item[Questions?] Dave assumes a certain amount of knowledge about electronics in his videos---he presents from a position of deep knowledge, and that can sometimes mean that everything isn't crystal clear for a beginner. What questions did you have during and after the video?
\end{description}

The last question is, perhaps, the most important. When you are reading new material, or watching tutorials online, you should always be writing down questions. Framing your confusion in terms of questions, and making note of them so you can research them later (or ask for help) is a critical part of the learning process when studying electronics.

\subsection{A Typical (Safe) Multimeter}

\newthought{Tools made by Fluke have a near-mythical regard in the electrical world.} They have, historically, made high-quality tools that are accurate, reliable, and have integrated safety features that make them appropriate for use in a wide variety of contexts. You might find some of their lower-end (or non-specialized) meters in classroom or other learning settings. The Fluke 115 and Fluke 117 meters are both examples of this non-specialized, general-purpose meter that you might encounter either in the classroom or in the field.

\begin{figure}
  \begin{centering}
    \includegraphics[]{content/measurement/images/fluke-115.jpg}
  \end{centering}
  \caption{A Fluke 115 multimeter.}
  \label{ch1:fluke115}
\end{figure}

There are two important things to note about the meter:

\begin{description}
  \item[The probe connections.] There are three places to connect probes at the bottom of the multimeter.
  \item[The function select knob.] There are nine different functions on this multimeter that are selected by a large control knob.
\end{description}

You will always connect your black probe to the COM (meaning ``common'', or ``electrical ground'') port on the meter. The color of the wire does not, in truth, matter, but by convention we typically use a black wire to indicate electrical ground. (The concept of ``electrical ground'' is a concept that we will explore further, later.) The other two probe connection points are used for different modes of measurement. On the left the probe connection is labeled ``A,'' which is the symbol for \emph{amperes}, or \emph{amps}, which stands for \emph{electrical current}. On the left are all of the other symbols, and in particular, the symbol ``V,'' which stands for \emph{voltage}, which is where we will want to connect our red, or ``hot'' probe for the work that follows.

Second, you will want to turn your function select knob to the V with a pair of horizontal lines above it. This is typically the symbol for \emph{direct current voltage}, or DC voltage, and you will find some variation of this symbol on all multimeters. This is demonstrated below, schematically, with a diagram taken from the Fluke 114/115/117 manual (Figure~\vref{ch1:readdcvolts}).

\begin{figure}
  \begin{centering}
    \includegraphics[]{content/measurement/images/read-dc-volts.png}
  \end{centering}
  \caption{Reading AC or DC voltage with a Fluke multimeter.}
  \label{ch1:readdcvolts}
\end{figure}

Once you have connected the probes and dialed in the functionality that you desire, you can use your meter to measure DC voltages. In particular, you can test the power supply (remember the power supply?) by touching the black probe to the ground terminal of the supply, and the red probe to the positive output of the power supply. (These are typically black and red as well, by convention.) Your meter should, if all goes well, agree with what the power supply claims it is outputting.

To further reinforce this idea, and prepare to measure more voltages in more contexts, you should watch Martin Lorton's video on \href{https://www.youtube.com/watch?v=ZBbgiBU96mM}{How to use a Multimeter for Beginners}. This 30-minute video demonstrates how to use a multimeter to measure both AC and DC voltages in several different contexts.

Again, while you are watching this video, you should take notes:

\begin{description}
  \item[Process?] What is the correct process or series of steps you should follow when using a multimeter?

  \item[Safety?] What would appear to be the safety concerns you need to be aware of when using a multimeter? Does Martin lay them out concisely, or are you left interring them?

  \item[Gotchas?] Are there any things you might need to be particularly aware of that might trick you or otherwise lead to confusing readings when using a multimeter?

  \item[Questions?] Did Martin leave you with questions about multimeter safety and usage that you think still warrants further investigation or questioning? Did you go looking for answers to those questions, did you find any resources that were satisfying?

\end{description}

Make note of your answers in your electronics laboratory notebook. You should have one, you know, and in it should collect up all of the questions and resources that you find. Over time, it will become an invaluable resource unto itself.

\newpage

\section{In Conclusion: The Danger of the Routine}

\newthought{The most dangerous tool is the one you become comfortable and complacent with.} There is not a lot to using a multimeter; it is a relatively simple tool. Likewise, power supplies only have a few options on them: you dial in a voltage and you connect it to your electronics project. That said, they are both dangerous in multiple ways.

Your power supply is able to supply enough voltage and current to do harm to you, and certainly can harm your electronics projects. Handling it with respect, paying attention to how you have set it up, and connecting it to your projects in safe and correct ways is important to your safety as well as the safety of the components that you are working with. Some components, like LEDs and resistors, have almost no cash value. Other components, like accelerometers and GPS units, might cost anywhere from \$10 to \$50 or more. Further, these components might be very sensitive to over- or under-voltage supply, and incorrect use can damage or destroy them very quickly. Awareness and care are critical when working with a power supply.

Likewise, a multimeter is a simple tool, with only a few modes that you will use all the time. However, if you use the wrong mode at the wrong time (for example, by selecting DC voltage and attempting to measure AC voltage instead), you run the risk of damaging the multimeter. Similarly, it is possible that the cables you are using have become damaged through improper storage or careless use; you should always throw out damaged cables, as they represent a serious risk to the user of the meter. Used correctly and safely, a multimeter is an invaluable tool; used incorrectly, carelessly, or with the confidence of something that has become routine, and you could harm yourself or the system you are working on.

\newthought{Always approach every project as if it was new, every time.} When we become complacent with electricity and electrical systems, we become a danger to ourselves and others. Stay aware, stay focused, and never take electricity for granted.
