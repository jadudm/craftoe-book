\laboratory{Voltage Drops}

In this lab, we're going to explore the idea of a \emph{voltage drop}. As we will learn, the current in a circuit is constsant throughout, but the voltage drops as we make our way around the circuit. You may need to team up with someone else, as you'll often want two multimeters in this laboratory: one that is configured as an ammeter (to measure current) and a second configured as either a voltmeter or ohmmeter (to measure voltage and resistance).


\section{Two Resistors, in Series}

To begin, place two, 1K resistors in your breadboard in series (Figure~\ref{lab03:twor}). Measure their total resistance; it should be near 2K. Note the measured resistance in your lab notebook. As a reminder, you always measure resistance on an incomplete or otherwise broken circuit.

\begin{figure}[h]
  \begin{centering}
    \includegraphics[width=3in]{content/lab03/images/two-resistors.png}
    \caption{Two resistors in series.}
    \label{lab03:twor}
  \end{centering}
\end{figure}

Next, connect two more pieces to your circuit (not depicted).

\begin{description}
  \item[Power Supply] Get out your trusty power supply. You will go from the positive lead on your power supply to...
  \item[An Ammeter] Your multimeter (configured as an ammeter).
\end{description}

Your final circuit should go from the positive lead of your power supply to the positive lead of your ammeter. Then, from the negative lead of your ammeter to the red wire in Figure~\ref{lab03:twor}. Finally, from the black wire in the figure, you will go back to your power supply's negative, or low side. At this point, you have a complete circuit.

\sidenote{We know that we can burn out the resistors if we dial up the resistors too far. Assuming that we are using 1/4W, 1K resistors, what is the maximum voltage we can apply to a two resistor circuit? (P=IV and V=IR are your friend here.) Is it safe for us to work at 5V with this configuration?}

Now, you want to make several measurements.

\begin{enumerate}
  \item First, measure the current in this circuit. How many amps do you read in this two-resistor circuit?

  \item Next, measure the voltage between the red wire and the point in-between the two resistors. What is the voltage difference that you read across the first resistor?

  \item Next, measure the voltage between the middle of the two resistors and the black wire.

  \item Finally, measure the voltage difference between the red wire (in the diagram) and the black wire.

\end{enumerate}

If all went well, you should have read two voltages that add up to a third. If they don't, it should be very, very close.

\section{Two Resistors, In Series, With a Twist}

Now, we are going to reconfigure the circuit. It should be ordered as follows:

\begin{center}
Power Supply +V $\leftrightarrow$ Red Wire $\leftrightarrow$ 1K Resistor \\ $\leftrightarrow$ Ammeter $\leftrightarrow$ \\ 1K Resistor $\leftrightarrow$ Black Wire $\leftrightarrow$ Power Supply GND
\end{center}

In this configuration, the ammeter is in the middle of the two resistors. Dial in the same voltage you had appled before (for example, if it was 5V, then you should once again power your circuit at 5V), and carry out the following series of measurements.

\begin{enumerate}
  \item First, measure the current in this circuit. How many amps do you read in this two-resistor circuit?

  \item Next, measure the voltage between the red wire and the point between the first resistor and the ammeter. What is the  voltage that you read there? (Essentially, this is measuring the voltage difference across the first resistor.)

  \item Next, read the voltage between the ammeter and the other side of the second resistor. What is the voltage you read there? (Again, this is measuring the voltage difference across the second resistor.)

  \item Finally, measure the voltage difference between the red wire and the black wire.
\end{enumerate}

\subsection{Reflection on the Spot}

At this point, you want to have a discussion with your colleagues. The questions you want to consider:

\begin{enumerate}
  \item Did the voltage drop across the resistors change when you changed the position of the ammeter?

  \item Did the current in the circuit change when you changed the position of the ammeter?

  \item Do you think the current readings will be the same if you put it on the ``far'' side of the two resistors (eg. between the second resistor and the ground side of the power supply)? Why or why not?
  \end{enumerate}

Ultimately, we are exploring how voltage drops across a circuit, and whether or not current changes throughout the circuit.

\section{Two Different Resistors}

Leaving your ammeter where it is, remove one of your 1K resistors and replace it with a 2.2K resistor. The total resistance should, of course, be somewhere around 3.2K\ohm.

\begin{enumerate}
  \item Has the current in the circuit changed?
  \item What does V=IR say about this circuit? (Does it match your measurements?)
  \item What is the voltage drop across the 1K resistor?
  \item What is the voltage drop across the 2.2K resistor?
  \item Why do you think those measurements come out that way?
\end{enumerate}

\subsection{Reflection on the Spot}

You can, at this point, get one more resistor... perhaps a 560\ohm resistor (so you have a 1K and a 560\ohm in series), or you could go the other way, and put a 1K and a 10K in series. Your goal, with your partners, is to develop a theory about what is going on with the voltage as you measure it across the resistors. Do you see a larger drop across larger resistors? A smaller drop across larger resistors? Why do you think you are observing what you are observing?
