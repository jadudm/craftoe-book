\chapter{Exploring VIR}

\newthought{It is traditional to read all kinds of theoretical ideas} about voltage, current, and resistance before actually \emph{doing} anything. We're going to take a different approach here: instead, you're going to read about and conduct an experiment, and using the data from that experiment, extract the theory of how voltage, current, and resistance are related.

% CC-BY https://flic.kr/p/j2ZGvi.
\begin{marginfigure}
  \includegraphics[]{content/lab02/images/vir.jpg}
  \caption{Vir was a character on Babylon 5, played by Stephen Furst.}
  \label{lab02:vircotto}
\end{marginfigure}

When working with these three values mathematically, we refer to voltage as V, current as I (from the French, \emph{intensit\'e}), and resistance as R. You can remember these three together as VIR, and Stephen Furst (Figure~\ref{lab02:vircotto}) can help us with that.

\section{Experiment 1: One Resistor}

To begin, we'll take a breadboard and insert one resistor into it. For the moment, pick a 220\ohm resistor. Next, check it: use your multimeter to measure the resistance between the red wire and the black wire. It should be very close to 220\ohm. To do this, you'll need to make sure that your probes are in the correct location on your meter, and that you've selected the correct function for reading resistance. On the Fluke 117, that means the red probe will be on the right (where the \ohm symbol is), and the function select knob will be dialed around to \ohm.

\begin{framed}
It may seem a bit silly to be checking the resistance of the resistor. After all, you probably made sure to pick the correct one in the first place! However, someone may have put the wrong resistor back in the wrong place, and as a result, if you're not paying attention, you could build your circuit with the wrong components. Hence, you should always check.
\end{framed}

Next, we're going to switch our multimeter into \emph{ammeter mode}. This will transform it into a device that can measure electrical current. Figure~\vref{lab02:fluke} comes from the Fluke 117 manual, and shows how to configure your multimeter to measure current. The common probe remains in the middle, and the measurement probe (or red probe) moves to the left-hand port, which has an ``A'' next to it. The ``A'' stands for \emph{ampres} or (more commonly) \emph{amps}, which is the unit of measurement for electrical current.

\begin{figure}
  \begin{centering}
    \includegraphics[width=\textwidth]{content/lab02/images/fluke-ammeter-configuration.png}
    \caption{Configuring your multimeter as a ammeter.}
    \label{lab02:fluke}
  \end{centering}
\end{figure}

\subsection{Wiring the Circuit}

\begin{figure}
  \begin{centering}
    \includegraphics[width=3in]{content/lab02/images/ammeter-01.png}
    \caption{The multimeter in ammeter mode.}
    \label{lab02:ammeter}
  \end{centering}
\end{figure}

Once you have moved your probes around and rotated your selector around to \textbf{A} (specifically, DC amps), you'll need to wire everything together (Figure~\vref{lab02:ammeter}). Alligator clips are your friend here. Clip from your power supply's positive (red) output to the red probe, and from the black probe of the multimeter to the orange wire.\marginnote{In these diagrams, we use the battery to indicate ``power supply.'' You could, as it happens, use batteries as power supplies... or, you can use a bench-top power supply. We assume you're using a bench-top supply.} Then, clip from the black wire back to the negative (or black) input. In doing this, you are making the multimeter part of the circuit. We would say that the multimeter is \emph{in series} with the resistor.


With this configuration, we're ready to collect some data.

\subsection{Collecting Data: 220\ohm}

Momentarily disconnect your power supply, turn it on, and dial it down to 1V. Turn it off, reconnect it, and turn it back on. What you now want to do is collect 8 data points between one and five volts.

\begin{enumerate}
  \item Let the ammeter stabilize.
  \item Read the current from the ammeter.
  \item Increase the voltage by 0.5 volts.
  \item Repeat for 1.5V, 2V, ... up to 5V.
\end{enumerate}

When you are done, you should have two columns of numbers: a column containing voltages and a column containing amperage readings. Next, \emph{neatly} sketch a graph in your notebook (or, on paper, and insert a photo into your digital notebook)

\subsection{Collecting Data: 1000\ohm}

Repeat the experiment from the previous section with a 1K\ohm resistor. Again, sketch the resulting graph in your lab notebook.

\subsection{Collecting Data: 2.2K\ohm}

Finally, repeat the experiment one more time for a 10K resistor. Again, sketch the resulting graph in your lab notebook.

\subsection{Reflection On The Spot}

You've just completed three data gathering exercises. You should have three graphs showing the relationship between the voltage you apply to your circuit and the current that flows through it. What observations do you and your partner have about the relationship between these values at this point?

Make notes of your reflections on this data in your notebook at this point. Your thoughts, right now, will likely inform your writeup later, and it would be wise to capture your thinking ``in the moment'' before you proceed.

\section{Experiment 2: Multiple Resistors}

We have one more experiment to carry out involving resistors. This one will involve two different configurations of our meter: one for measuring resistance, and one for measuring current.

We begin by reconfiguring our meter for measuring resistance. Also, this part of our exploration will require several 1K resistors, so you might grab three now so you're ready to go.

\subsection{One Resistor: 1K}

Insert one, 1K resistor into the breadboard. Measure its resistance, and record this in your notebook.

\subsection{Two Resistors: 1K}

Insert two, 1K resistors in your breadboard as depicted. Measure the resistance you find from one end to the next. Record this in your lab notebook.
