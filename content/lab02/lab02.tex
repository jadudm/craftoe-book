\laboratory{Exploring Voltage, Current, and Resistance}

\newthought{It is traditional to read all kinds of theoretical ideas} about voltage, current, and resistance before actually \emph{doing} anything. We're going to take a different approach here: instead, you're going to read about and conduct an experiment, and using the data from that experiment, extract the theory of how voltage, current, and resistance are related.

\section{Pre-Lab Questions}

There is no pre-lab for this experiment. This lab continues working with resistors and the meter. While it does introduce new concepts, no new concepts are needed to understand the work of the lab itself.

\section{Experiment 1: One Resistor}

To begin, we'll take a breadboard and insert one resistor into it. For the moment, pick a 220\ohm resistor. Next, check it: use your multimeter to measure the resistance between the red wire and the black wire. It should be very close to 220\ohm. To do this, you'll need to make sure that your probes are in the correct location on your meter, and that you've selected the correct function for reading resistance. On the Fluke 117, that means the red probe will be on the right (where the \ohm symbol is), and the function select knob will be dialed around to \ohm.

\begin{figure}
  \begin{centering}
    \includegraphics[width=\textwidth]{content/lab02/images/fluke-ammeter-configuration.png}
    \caption{Configuring your multimeter as a ammeter.}
    \label{lab02:fluke}
  \end{centering}
\end{figure}

Next, we're going to switch our multimeter into \emph{ammeter mode}. This will transform it into a device that can measure electrical current. Figure~\vref{lab02:fluke} comes from the Fluke 117 manual, and shows how to configure your multimeter to measure current. The common probe remains in the middle, and the measurement probe (or red probe) moves to the left-hand port, which has an ``A'' next to it. The ``A'' stands for \emph{ampres} or (more commonly) \emph{amps}, which is the unit of measurement for electrical current.

\subsection{Wiring the Circuit}

Once you have moved your probes around and rotated your selector around to \textbf{A} (specifically, DC amps), you'll need to wire everything together. Alligator clips are your friend here. Clip from your power supply's positive (red) output to the red probe, and from the black probe of the multimeter to the red wire.\marginnote{In these diagrams, we use the battery to indicate ``power supply.'' You could, as it happens, use batteries as power supplies... or, you can use a bench-top power supply. We assume you're using a bench-top supply.} Then, clip from the black wire back to the negative (or black) input. In doing this, you are making the multimeter part of the circuit. We would say that the multimeter is \emph{in series} with the resistor.

\begin{figure}
  \begin{centering}
    \includegraphics[width=4in]{content/lab02/images/ammeter-01.png}
    \caption{The multimeter in ammeter mode.}
    \label{lab02:ammeter}
  \end{centering}
\end{figure}

With this configuration, we're ready to collect some data.

\subsection{Collecting Data: 220\ohm}

Momentarily disconnect your power supply, turn it on, and dial it down to zero volts. Turn it off, reconnect it, and turn it back on.\marginnote{This should be your standard operating procedure, always. That is, you should disconnect your power supply, set the voltage, turn it off, then reconnect.} What you now want to do is collect 10 data points between zero and five volts.

\begin{enumerate}
  \item Let the ammeter stabilize. Record the voltage and current in a data table.
  \item Increase the voltage by 0.5 volts.
  \item Repeat for 0.5V, 1.0V, 1.5V, ... up to 5V.
\end{enumerate}

When you are done, you should have two columns of numbers: a column containing voltages and a column containing amperage readings. Next, \emph{neatly} sketch a graph in your notebook (or, on paper, and insert a photo into your digital notebook)

\subsection{Collecting Data: 1000\ohm}

Repeat the experiment from the previous section with a 1K\ohm resistor.

\subsection{Collecting Data: 2.2K\ohm}

Repeat the experiment from the previous section with a 2.2K\ohm resistor.

\subsection{Reflection On The Spot}

You've just completed three data gathering exercises. You should have three graphs showing the relationship between the voltage you apply to your circuit and the current that flows through it. What observations do you and your partner have about the relationship between these values at this point?

Make notes of your reflections on this data in your notebook at this point. Your thoughts, right now, will likely inform your writeup later, and it would be wise to capture your thinking ``in the moment'' before you proceed.

\section{Experiment 2: Multiple Resistors}

We have one more experiment to carry out involving resistors. This one will involve two different configurations of our meter: one for measuring resistance, and one for measuring current.

We begin by reconfiguring our meter for measuring resistance. Also, this part of our exploration will require several 1K resistors, so you might grab three now so you're ready to go.

\subsection{One Resistor: 1K}

Insert one, 1K resistor into the breadboard. Measure its resistance, and record this in your notebook.

\subsection{Two Resistors: 1K}

Insert two, 1K resistors in your breadboard as depicted. Measure the resistance you find from one end to the next. Record this in your lab notebook.

\begin{figure}
  \begin{centering}
    \includegraphics[width=4in]{content/lab02/images/two-resistors.png}
    \caption{Resistors in a row.}
    \label{lab02:series2}
  \end{centering}
\end{figure}

\subsection{Three Resistors: 1K}

Insert three, 1K resistors in your breadboard as depicted. Measure and record.

\begin{figure}
  \begin{centering}
    \includegraphics[width=4in]{content/lab02/images/three-resistors.png}
    \caption{Resistors in a row.}
    \label{lab02:series3}
  \end{centering}
\end{figure}

\subsection{Reflection On The Spot}

You've once again completed a short data gathering run. What do you observe? What behavior of the physical system seems to be emerging?

Repeat one or two of these sequencing experiments with a different value resistor to make sure this isn't a special property of 1K resistors. (In other words, test your hypothesis.)

\section{Experitment 3: Changing the Configuration}

For your second set of experiments, you're going to change your resistor configuration.

\begin{figure}
  \begin{centering}
    \includegraphics[width=4in]{content/lab02/images/parallel.png}
    \caption{Resistors side-by-side.}
    \label{lab02:parallel}
  \end{centering}
\end{figure}

In this configuration, the resistors are side-by-side. This time, just measure the resistance.

\subsection{Collecting Data: Two Resistors}

Measure and record the resistance for pairs of resistors at 220\ohm, 1K\ohm, and 10K\ohm.

\subsection{Collecting Data: Three Resistors}

Measure and record the resistance for triplets of resistors at 220\ohm, 1K\ohm, and 10K\ohm.

\subsection{Reflection on the Spot}

You may need to do more exploration of the paired arrangement of resistors before they make sense. Make note of your findings so far, and record your hypothesis as to what you think is happening in the physical system you are exploring.

\section{Reflection}

This is not meant to be a huge exploration. What makes it interesting is puzzling out what is happening to the resistance when you line them up (or put them in \emph{series}) and when you put them side-by-side (in \emph{parallel}). There is a mathematical way to describe what happens in each case.

Your challenge, with your partner, is to see if you can come up with the mathematical pattern that the resistances are following. For series, it should be somewhat obvious. For resistors arranged in parallel, however, you might have to think a bit upside-down, or even collect more data.

It is certainly the case that you can look this up online. Don't. You'll be reading about it soon enough, but for now, see if you can develop a theory that explains the behavior of the resistors and how they combine, mathematically, when arranged in series and parallel.

Make note of your theories in your lab notebook.
