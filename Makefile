DATE=`date +"%Y%m%d"`

all: clean
	pdflatex -halt-on-error main
	makeindex main
	pdflatex -halt-on-error main
.PHONY : all

upload: all
	scp main.pdf craftoeorg@craftofelectronics.org:~/craftofelectronics.org/docs/book/jadudm-craftoe-${DATE}.pdf
	echo ""
	echo "jadudm-craftoe-${DATE}.pdf"
	echo ""

clean:
	rm -f main.pdf
	rm -f *.fdb_latexmk
	rm -f *.fls
	rm -f *.blg
	rm -f *.aux
	rm -f *.idx
	rm -f *.ind
	rm -f *.ilg
	rm -f *.lof
	rm -f *.log
	rm -f *.lot
	rm -f *.out
	rm -f *.toc
